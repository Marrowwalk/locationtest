//
//  Locaton+CoreDataProperties.m
//  LocationTest
//
//  Created by Kos on 15/12/2016.
//  Copyright © 2016 Kos Marrowwalk. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "Locaton+CoreDataProperties.h"

@implementation Locaton (CoreDataProperties)

+ (NSFetchRequest<Locaton *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Locaton"];
}

@dynamic category;
@dynamic date;
@dynamic latitude;
@dynamic locationDescription;
@dynamic longitude;
@dynamic placemark;

@end
