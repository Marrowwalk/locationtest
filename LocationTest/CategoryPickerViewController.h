//
//  CategoryPickerViewController.h
//  LocationTest
//
//  Created by Kos on 12/12/2016.
//  Copyright © 2016 Kos Marrowwalk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryPickerViewController : UITableViewController

@property (nonatomic, strong) NSString *selectedCategoryName;

@end
