//
//  FirstViewController.m
//  LocationTest
//
//  Created by Kos on 05/12/2016.
//  Copyright © 2016 Kos Marrowwalk. All rights reserved.
//

#import "CurrentLocationViewController.h"
#import "LocationDetailsViewController.h"

@interface CurrentLocationViewController ()

@end

@implementation CurrentLocationViewController {
    CLLocationManager *_locationManager;
    CLLocation *_location;
    BOOL _updatingLocation;
    NSError *_lastLocationError;
    CLGeocoder *_geocoder;
    CLPlacemark *_placemark;
    BOOL _performingReverseGeocoding;
    NSError *_lastGeocodingError;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        _locationManager = [[CLLocationManager alloc] init];
        _geocoder = [[CLGeocoder alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self updateLabels];
    [self configureGetButton];
}

- (IBAction)getLocation:(id)sender {
    if (_updatingLocation) {
        [self stopLocationManager];
    }
    else {
        _location = nil;
        _lastLocationError = nil;
        [self startLocationManager];
        _placemark = nil;
        _lastGeocodingError = nil;
        [self startLocationManager];
    }
    [self updateLabels];
    [self configureGetButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CLLocationManagerDelegate

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError %@", error);
    if (error.code ==kCLErrorLocationUnknown) {
        return;
    }
    [self stopLocationManager];
    _lastLocationError = error;
    
    [self updateLabels];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"TagLocation"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        LocationDetailsViewController *controller = (LocationDetailsViewController *) navigationController.topViewController;
        
        controller.coordinate = _location.coordinate;
        controller.placemark = _placemark;
    }
}

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *newLocation = [locations lastObject];
    NSLog(@"didUpdateLocations %@", newLocation);
    
    if ([newLocation.timestamp timeIntervalSinceNow] < -5.0) {
        return;
    }
    if (newLocation.horizontalAccuracy < 0) {
        return;
    }
    CLLocationDistance distance = MAXFLOAT;
    if (_location != nil) {
        distance = [newLocation distanceFromLocation:_location];
    }
    if (_location == nil || _location.horizontalAccuracy > newLocation.horizontalAccuracy) {
        
    _lastLocationError = nil;
    _location = newLocation;
    [self updateLabels];
        if (newLocation.horizontalAccuracy <= _locationManager.desiredAccuracy) {
            NSLog(@"It's done!");
            [self stopLocationManager];
            [self configureGetButton];
            if (distance > 0) {
                _performingReverseGeocoding = NO;
            }
        }
        if (!_performingReverseGeocoding) {
            NSLog(@"Going to geocode");
            _performingReverseGeocoding = YES;
            [_geocoder reverseGeocodeLocation:_location completionHandler:^(NSArray *placemarks,  NSError *error) {
                NSLog(@"Found placemarks %@, error: %@", placemarks, error);
                _lastLocationError = error;
                if (error == nil && [placemarks count] > 0) {
                    _placemark = [placemarks lastObject];
                }
                else {
                    _placemark = nil;
                }
                _performingReverseGeocoding = NO;
                [self updateLabels];
            }];
        }
        else if (distance < 1.0) {
            NSTimeInterval timeInterval = [newLocation.timestamp timeIntervalSinceDate:_location.timestamp];
            if (timeInterval > 10) {
                NSLog(@"Force done!");
                [self stopLocationManager];
                [self updateLabels];
                [self configureGetButton];
            }
        }
    }
}

- (void) updateLabels {
    if (_location != nil) {
        self.latitudeLabel.text = [NSString stringWithFormat:@"%.8f", _location.coordinate.latitude];
        self.longitudeLabel.text = [NSString stringWithFormat:@"%.8f", _location.coordinate.longitude];
        self.tagButton.hidden = NO;
        self.messageLabel.text = @"";
        
        if (_placemark != nil) {
            self.addressLabel.text = [self stringFromPlacemark: _placemark];
        }
        else if (_performingReverseGeocoding) {
            self.addressLabel.text = @"Searching for address..";
        }
        else if (_lastGeocodingError != nil) {
            self.addressLabel.text = @"Error finding address";
        }
        else {
            self.addressLabel.text = @"No address found";
        }
    }
    else {
        self.latitudeLabel.text = @"";
        self.longitudeLabel.text = @"";
        self.addressLabel.text = @"";
        self.tagButton.hidden = YES;
        self.messageLabel.text = @"Press the Button to Start";
        
        NSString *statusMessage;
        if (_lastLocationError != nil) {
            if ([_lastLocationError.domain isEqualToString:kCLErrorDomain] && _lastLocationError.code == kCLErrorDenied) {
                statusMessage = @"Location disabled";
            }
            else {
                statusMessage = @"Error getting location";
            }
        }
        else if (![CLLocationManager locationServicesEnabled]) {
            statusMessage = @"Location service disabled";
        }
        else if (_updatingLocation) {
            statusMessage = @"Searching...";
        }
        else {
            statusMessage = @"Press the button to start";
        }
        self.messageLabel.text = statusMessage;
    }
}

- (NSString *) stringFromPlacemark: (CLPlacemark *)thePlacemark {
    return [NSString stringWithFormat:@"%@ %@\n%@ %@ %@", thePlacemark.subThoroughfare, thePlacemark.thoroughfare, thePlacemark.locality, thePlacemark.administrativeArea, thePlacemark.postalCode];
}

- (void) configureGetButton {
    if (_updatingLocation) {
        [self.getButton setTitle:@"Stop" forState:(UIControlStateNormal)];
    }
    else {
        [self.getButton setTitle:@"Get my Location" forState:UIControlStateNormal];
    }
}

- (void) startLocationManager {
    if ([CLLocationManager locationServicesEnabled]) {
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [_locationManager startUpdatingLocation];
        _updatingLocation = YES;
        [self performSelector:@selector(didTimeOut:) withObject:nil afterDelay:60];
    }
}

- (void) stopLocationManager {
    if (_updatingLocation) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(didTimeOut:) object:nil];
        [_locationManager stopUpdatingLocation];
        _locationManager.delegate = nil;
        _updatingLocation = NO;
    }
}

- (void) didTimeOut: (id) obj{
    NSLog(@"Time out");
    if (_location == nil ) {
        [self stopLocationManager];
        
        _lastLocationError = [NSError errorWithDomain:@"MyLocationsErrorDomain" code:1 userInfo:nil];
        [self updateLabels];
        [self configureGetButton];
    }
}

@end
