//
//  HudView.m
//  LocationTest
//
//  Created by Kos on 14/12/2016.
//  Copyright © 2016 Kos Marrowwalk. All rights reserved.
//

#import "HudView.h"

@implementation HudView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    
    const CGFloat hudWidth = 96.0f;
    const CGFloat hudHeight = 96.0f;
    CGRect hudRect = CGRectMake(roundf(self.bounds.size.width - hudWidth) / 2.0f, roundf(self.bounds.size.height - hudHeight)  / 2.0f, hudWidth, hudHeight);
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:hudRect cornerRadius:10];
    [[UIColor colorWithWhite:0.3f alpha:0.8f] setFill];
    [roundedRect fill];
    UIImage *image = [UIImage imageNamed:@"Checkmark"];
    CGPoint imagePoint = CGPointMake(self.center.x - roundf(image.size.width / 2.0f), self.center.y - roundf(image.size.height / 2.0f) - hudHeight / 9.0f);
    [image drawAtPoint:imagePoint];
    NSDictionary *attributes = @{
                                 NSFontAttributeName: [UIFont systemFontOfSize: 16.0f],
                                 NSForegroundColorAttributeName: [UIColor whiteColor]
                                 };
    CGSize textSize = [self.text sizeWithAttributes:attributes];
    CGPoint textPoint = CGPointMake(self.center.x - roundf(textSize.width / 2.0f), self.center.y - roundf(textSize.height / 2.0f) + hudHeight / 4.0f);
    [self.text drawAtPoint:textPoint withAttributes:attributes];
                                 
}

- (void) showAnimated: (BOOL) animated {
    if (animated) {
        // setting up initial state before animation
        self.alpha = 0.0f;
        self.transform = CGAffineTransformMakeScale(1.3f, 1.3);
    }
    // animation block calling
    [UIView animateWithDuration:0.3f animations:^{
        self.alpha = 1.0f;
        self.transform = CGAffineTransformIdentity;
    }];
}

+ (instancetype) hudInView:(UIView *)view animated:(BOOL)animated {
    HudView *hudView = [[HudView alloc] initWithFrame:view.bounds];
    hudView.opaque = NO;
    [view didAddSubview:hudView];
    view.userInteractionEnabled = NO;
    hudView.backgroundColor = [UIColor colorWithRed:1.0f green:0
                                               blue:0 alpha:0.5f];
    [hudView showAnimated:animated];
    return hudView;
}

@end
