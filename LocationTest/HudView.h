//
//  HudView.h
//  LocationTest
//
//  Created by Kos on 14/12/2016.
//  Copyright © 2016 Kos Marrowwalk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HudView : UIView

+ (instancetype) hudInView: (UIView *) view animated: (BOOL) animated;
@property (nonatomic, strong) NSString *text;

@end
