//
//  LocationDetailsViewController.h
//  LocationTest
//
//  Created by Kos on 07/12/2016.
//  Copyright © 2016 Kos Marrowwalk. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <CoreLocation/CoreLocation.h>

@interface LocationDetailsViewController : UITableViewController

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) CLPlacemark *placemark;

@end
