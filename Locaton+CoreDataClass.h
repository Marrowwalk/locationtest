//
//  Locaton+CoreDataClass.h
//  LocationTest
//
//  Created by Kos on 15/12/2016.
//  Copyright © 2016 Kos Marrowwalk. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface Locaton : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Locaton+CoreDataProperties.h"
