//
//  Locaton+CoreDataProperties.h
//  LocationTest
//
//  Created by Kos on 15/12/2016.
//  Copyright © 2016 Kos Marrowwalk. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "Locaton+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Locaton (CoreDataProperties)

+ (NSFetchRequest<Locaton *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *category;
@property (nullable, nonatomic, copy) NSDate *date;
@property (nonatomic) double latitude;
@property (nullable, nonatomic, copy) NSString *locationDescription;
@property (nonatomic) double longitude;
@property (nullable, nonatomic, retain) CLPlacemark *placemark;

@end

NS_ASSUME_NONNULL_END
